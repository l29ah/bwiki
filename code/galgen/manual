h1. How to use Galgen to generate a gallery

If you feel like learning by example, here's my own gallery source: [http://git.bitcheese.net/?a=tree&p=art_thou&h=HEAD]. Otherwise, read on.

h2. Directories

First, pick a directory to contain your gallery data. You may want to add it to version control.

Second, place images in directory "images". You may name them whatever you want, and if you want them to appear in certain order when user is browsing gallery, you can prepend numbers like "001_myimage.jpeg".

To add a custom title and/or description to image, create a file in directory "descriptions". Filename of a description should consist of image's base name (stripped of number_) and ".textile" at end. So, file with description for image "images/001_myimage.jpeg" should be named "descriptions/myimage.jpeg.textile".

To add a custom title and/or description to gallery, create similar file named "index.textile".

Description file's format is as follows: image title, double \n, image description (which is in "Textile":http://redcloth.org/textile).

Example:

bc.. A random photo

It's me and my friend, reading "Wikipedia":http://wikipedia.org

p. Any gallery can include child galleries, they should be placed in their own subdirectories in directory "gallery". Child galleries can have all of subdirectories mentioned above.

h2. Root-only files

These files can be placed in root directory of gallery only:

*static* directory: this folder will be simply copied into output directory.

*galgen.yml* file: contains Galgen's "configuration":#config.

*gallery.erb*, *gallery_preview.erb*, *image.erb*, *image_preview.erb*: page "templates":#templates.

h2(#config). Config

Config is in "YAML":http://www.yaml.org/ format and placed in *galgen.yml* file in gallery root.

|_. Field |_. Default |_. Description |
| :thumb_size | 800x600 | Thumbnail size |
| :minithumb_size | 200x150 | Minithumbnail size (for gallery preview) |
| :atom_items | 10 | Number of last items in Atom feed |
| :total_feed | gindex.xml | Name of feed with updates to all galleries |
| :http_base | | HTTP base that will be prepended to URIs in Atom feed |

h2(#templates). Templates

Using templates you can totally (well, somewhat) change the look and layout of your gallery. Templates are in "ERB":http://www.ruby-doc.org/stdlib/libdoc/erb/rdoc/ format and have certain variables available to them. Default templates are created by Galgen if you don't have any in gallery directory. Templates are:

*gallery.erb*: Defines the look of gallery page.
*gallery_preview.erb*: Defines the look of gallery preview (these show up on gallery page, in child galleries' section).

*image.erb*: Defines the look of individual image pages.
*image_preview.erb*: Defines the look of image preview on gallery page.

Variables available for all templates:

*gallery_title*: Galley title, either taken from gallery description or from its filename.
*gallery_url*: Link to gallery page.
*root*: Link to gallery root.
*gallery_path*: Array of parent galleries, begins from root.

Variables available only for image templates:

*image_description*: Image description
*image_title*: Image title, either taken from image description or from its filename.
*image_page_url*: Link to image page.
*image_url*: Link to original image.
*image_thumb_url*: Link to image thumbnail.
*image_minithumb_url*: Link to image mini-thumbnail.
*image_name*: Cleaned image name, without number or extension.
*modified*: DateTime of last image modification.
*original_name*: Unstripped name of image, with number, if any, and extension.
*next_image_url*: Link to next image page, if any, *nil* otherwise.
*prev_image_url*: Link to previous image page, if any, *nil* otherwise.

h2. Running

Now, run @galgen@ with first parameter being your current directory, second being output directory (will be created if not exists):

bc. % galgen my_gallery my_html
<bunch of output here>

That's it, your gallery is created in my_html! Just copy it over to HTTP server and enjoy.