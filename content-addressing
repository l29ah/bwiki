h1. Content-addressing links and why they matter

h2. Location addressing

Location addressed links look like this:

bc. https://someserver.info/some/path/somefile.zip?somerandomjunk

Basically, such links tells you "If you use that protocol to access this service and send it this query, you will receive certain information".

Traditionally, when the internet was built, everybody thought links like this are permanent. Lots of software was built on the idea that links will be around forever and can be fetched any time. Good example are XSD xmlns links, they use http:// links to identify schemas, with the idea that they can be fetched any time to get needed information. In practice, these links die.

h3. Links break

Location-addressed links are incredibly flimsy things. You need:

1. Somebody to still pay for the hostname
2. Somebody to still pay for the hosting
3. These people being the same-ish as before
4. Information not being purged or just moved without leaving behind a redirect in a random site reorganization.

And nowadays, these 4 are often not the case.

h3. Control

When somebody makes you use location-adressed links, they basically want to control how you access this information, and ensure that all requests go through them. It's very cheap for big entities to supply you with free space for your files, with the payoff being that you are dependent on them, and if you decide to move, your links would break at the very least. Also even if they are not malevolent, they might just decide that keeping your information or notifying you that it's going away soon is just not worth the trouble.

h2. Content addressing

Content addressed links contain information about the content of the link. They don't look as nice as location adressed ones:

bc. /ipfs/zb2rhfzhZRvqzXbwygrubN9aMdzABdfNeKcerEZRT9ZnX19TH

What this link does, though, is contain "cryptographic hash":https://en.wikipedia.org/wiki/Cryptographic_hash_function of the data I link to. It tells you "If you ask around on ipfs network, you will find the exact data I linked you". It's not reliant on some specific computer, it's not possible to court-order somebody to completely disable this link, it's not possible to substitute the data with something else.

When you link to data in content-addressable way, you release the control of it and let other people host it. If you care about the data and people you share it with, use content addressing.

h2. Further reading

* "Practical IPFS overview":ipfs-overview
