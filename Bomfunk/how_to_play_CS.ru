h1(#howtoplayCS). Как научиться играть в Counter-Strike

Есть люди, которые хотели бы научиться играть лучше в "Counter-Strike":http://ru.wikipedia.org/wiki/Counter-Strike. А мне хотелось бы помочь им, но я, пожалуй, не смогу каждому рассказывать одно и то же по многу раз. Поэтому постараюсь описать основные принципы в этой статье. По умолчанию я буду предполагать, что Вы уже немного ознакомились с данной игрой, поэтому не буду, например, рассказывать, какие оружия в игре есть.

Кстати, предупреждаю сразу: используйте эту статью на свой страх и риск. В ней выражено моё субъективное мнение, которое может не совпадать с мнением других игроков или быть неправильным ввиду недостатка игрового опыта. Но те, кто видел качество моей игры и его оно привлекло, может смело пользоваться этой статьёй, так как здесь я раскрываю свои секреты.

Итак, как же научиться хорошо играть в "Counter-Strike":http://ru.wikipedia.org/wiki/Counter-Strike? Думаю, начать стоит с конфигурации игры.

toc.

h2(#configuration). Конфигурация

Перво-наперво необходимо решить, нужна ли Вам акселерация мыши, или нет. Что это такое, можно прочитать "здесь":http://aghl.ru/wiki/index.php?title=%D0%90%D0%BA%D1%81%D0%B5%D0%BB%D0%B5%D1%80%D0%B0%D1%86%D0%B8%D1%8F_%D0%BC%D1%8B%D1%88%D0%B8, но я могу объяснить кратко. Когда акселерация включена, расстояние, пройденное прицелом в игре, зависит от скорости движения мыши. То есть, оно будет маленьким, если мышью двигать медленно, и большим, если ей двигать быстро. На мой взгляд, это очень мешает играть - ситуации в игре бывают разными, целиться иногда нужно очень быстро, для этого нужно чувствовать соответствие движения мыши на коврике и прицела на экране. Лучше, когда независимо от скорости движения какое-то расстояние, пройденное мышью на коврике, точно равно какому-то расстоянию, пройденному прицелом на экране. В общем, я рекомендую всегда отключать акселерацию мыши. Делается это следующим образом.

Сначала отключим акселерацию в CS. Для этого нужно добавить следующие параметры запуска:

pre. -noforcemaccel -noforcemparms -noforcemspd

Если у вас non-steam CS, то эти параметры можно добавить в свойствах ярлыка справа от hl.exe, а если у Вас лицензионная версия игры, то это делается проще - в свойствах игры в библиотеке Steam есть возможность добавить дополнительные параметры запуска.

Теперь отключим акселерацию в Windows. Для этого необходимо зайти в *Панель управления -> Мышь* (возможно, расположение этих настроек в Вашей версии Windows отличается, в этом случае попробуйте найти настройки мыши самостоятельно). Попереключайтесь по вкладкам и найдите флажок "*Включить повышенную точность установки указателя*". Отключите эту функцию. На работе в Windows это практически никак не отразится, можете не переживать.

Теперь поговорим о "вертикальной синхронизации":http://ru.wikipedia.org/wiki/%D0%92%D0%B5%D1%80%D1%82%D0%B8%D0%BA%D0%B0%D0%BB%D1%8C%D0%BD%D0%B0%D1%8F_%D1%81%D0%B8%D0%BD%D1%85%D1%80%D0%BE%D0%BD%D0%B8%D0%B7%D0%B0%D1%86%D0%B8%D1%8F. Опять же, если говорить кратко, это функция, ограничивающая количество обрабатываемых кадров за промежуток времени до поддерживаемого монитором количества. Всё бы хорошо, но когда эта функция включена, лично я замечаю следующий эффект: изображение на экране немножко опаздывает за Вашими действиями. То есть, прицел не мгновенно смещается при передвижении мыши по коврику, а будто ему нужно время, чтобы среагировать и обработать Ваш запрос. При этом FPS (frames per second) в игре ограничивается где-то до 60-70. Но так дело не пойдёт, запомните: нам нужно 99-100.

Смело пишите в игровой консоли команду:

pre. fps_max 101

Чем больше, тем лучше, но если не включен режим developer, игра всё равно не даст нам больше 100 fps.

Но это ещё не всё. Если у Вас NVIDIA, то Вам необходимо зайти в *Панель управления NVIDIA -> Параметры 3D* и для приложения Half-Life (можно найти hl.exe вручную) отключить *Вертикальный синхроимпульс*.

Всё? Нет. Теперь нужно настроить сетевые параметры в игре.

pre. net_graph 3
net_graphpos 3

Такой настройкой пользуюсь я. Это включает мониторинг FPS, скорости передачи информации, пинга и потерь пакетов в левом нижнем углу экрана. Если вам не нравится позиция, попробуйте разные значения net_graphpos.

Теперь настроим сеть. cl_cmdrate - это количество команд, передаваемых на сервер в секунду. cl_updaterate - то же самое, только на приём. rate - общее ограничение по трафику, ex_interp - интерполяция (подробнее "тут":http://2po.eu/blog/ex_interp-cheat-or-not/). Установим эти значения по максимумам:

pre. rate 25000
cl_cmdrate 101
cl_updaterate 101
ex_interp 0.01

Counter-Strike - старая игра, и предусмотрена для плохих сетевых соединений. Но сегодня качество домашнего Интернета практически всегда позволяет такие настройки. Но всё же краткий инструктаж по поводу изменения этих параметров:

Следите за loss и choke на net_graph. Если у вас есть loss, то попробуйте уменьшить значение cl_updaterate, если есть choke - уменьшите cl_cmdrate. Если есть и то, и другое, уменьшите rate. А значение ex_interp должно быть равно 1/cl_updaterate, то есть, если cl_updaterate станет 50, то ex_interp будет 0,02.

Теперь две мелочи - в игре по умолчанию оружие, поднятое Вами, тут же оказывается у Вас в руках. Иногда это может очень помешать, особенно, если Вы поднимаете оставленный кем-то пустой AK-47 и прекращаете стрельбу из-за этой неудачи. Убрать автоматическое переключение на оружие можно командой

pre. _cl_autowepswitch "0"

Так же в Counter-Strike по умолчанию выбор оружия необходимо подтверждать нажатием кнопки выстрела. Это, во-первых, непривычно, во-вторых, требует дополнительного времени и не очень удобно. Сделать привычным переключение оружия можно командой

pre. hud_fastswitch "1"

Ещё один полезный совет: иногда в CS после сворачивания оружие почему-то переходит из правой руки в левую. Починить это можно следующей командой:

pre. cl_righthand 0 ; cl_righthand 1

Печатать каждый раз столько не очень-то удобно, поэтому можно исправление этого бага назначить на кнопку (у меня это F7):

pre. bind F7 "cl_righthand 0 ; cl_righthand 1"

Научитесь быстро закупать оружие. Это довольно индивидуальное дело, кто-то хорошо запоминает цифры в меню покупок на кнопку B, кто-то делает дополнительные бинды (назначения оружий на отдельные кнопки), я пользуюсь вторым способом. Приведу собственный пример.

pre. bind "KP_HOME" "vest"
bind "KP_UPARROW" "vesthelm"
bind "KP_PGUP" "defuser"
bind "KP_LEFTARROW" "ak47;m4a1"
bind "KP_5" "awp"
bind "KP_RIGHTARROW" "deagle"
bind "KP_END" "flash"
bind "KP_DOWNARROW" "hegren"
bind "KP_PGDN" "sgren"

Этот код присутствует в моём конфиге. Он занимает NUM-pad следующим образом:

pre. 1 - флешка (слепящая граната)
2 - HE (взрывающая)
3 - дым
4 - AK-47 или M4A1, в зависимости от стороны
5 - AWP
6 - Desert Eagle
7 - броня
8 - броня со шлемом
9 - щипцы (defuser kit)

В общем-то, назначения кнопок на клавиатуре - Ваше личное дело, здесь дайте волю собственной фантазии.

Кстати, на кнопку F1 происходит автоматическая закупка оружия. Но Вы можете изменить по своему усмотрению файл autobuy.txt в папке cstrike, чтобы эта функция работала иначе.

И последнее - sensitivity. Один человек когда-то учил меня, что прицеливаться нужно так быстро, чтобы это происходило одновременно со стрельбой. Для этого нужно настроить sensitivity (то есть чувствительность мыши) так, чтобы Ваша рука хорошо чувствовала её. Я много читал о том, как правильно настраивать sensitivity, и могу высказать собственное мнение.

Высокое sensitivity позволяет Вам быстро разворачиваться, а низкое - точно прицеливаться. Что же выбрать? Мой ответ - низкое. Рука хорошего игрока не должна быть ленивой, и в случае, если необходимо развернуться, она должна пройти большое расстояние на столе. Играть нужно так, чтобы запястье не лежало на столе, нужно, чтобы ходила вся рука. Почему? Потому что точность прицеливания очень важна, а возможность быстро развернуться остаётся даже при низкой чувствительности мыши, просто способ становится немного непривычным.

Если на Вашей мыши есть возможность изменить DPI, установите его как можно выше во время игры и запомните это значение. Кстати, более 1600 DPI ставить нет смысла, это уже и так очень хорошее значение. Теперь зайдите в игру и настройте sensitivity так, чтобы при прохождении мышью пути от левого края коврика до правого в игре Вы поворачивались на 180°. Немного побегайте и привыкните к такой низкой чувствительности. Теперь можно её немного подкорректировать для точности стрельбы.

Я обычно делаю это с ботом на какой-нибудь карте для прицеливания, например, aim_map. Как только бот покажется в поле зрения, необходимо мгновенно перевести на него свой прицел и выстрелить. Если прицел при таком резком движении не доходит до противника, то sensitivity необходимо увеличить, если прицел "убегает" слишком далеко - уменьшить. Зачем привыкать к чувствительности, когда чувствительность может привыкнуть к Вам? :)

h2(#shooting). Стрельба

Почти все знают, что самые универсальные и хорошие оружия в игре - это AK-47 и M4A1, а когда не хватает на них денег, можно купить famas или galil. Также в игре есть очень хорошее, но дорогое оружие, под названием AWP. Не все пользуются им по той причине, что им не так-то просто пользоваться, но если уж ты умудришься попасть с него в противника, то точно убьёшь, поэтому это оружие можно считать лучшим.

Итак, какие основные принципы необходимо знать при стрельбе из этих оружий?

Во-первых, никогда не смотрите в пол или в небо, если присутствует вероятность того, что сейчас из-за угла перед вами во всей красе предстанет противник и убьёт Вас выстрелом в голову. Прицел всегда необходимо держать там, где может недруг оказаться. Если Вы играете с "калашом" или "эмкой", держите прицел на уровне головы, чтобы наверняка попасть в неё, если она появится. Научиться чувствовать, на каком уровне располагается голова, несложно - тренируйтесь прицеливаться в голову своих тимейтов, на респе во время фризтайма или во время раунда, когда бежите сзади них. В случае с AWP прицел нужно держать немного ниже, потому что с него важно вообще попасть, а не попасть в голову. Главное, не попасть в ноги - тогда у противника останется 16 HP.

Во-вторых, научитесь не паниковать при виде противника и не зажимать кнопку огня до тех пор, пока он не умрёт. Стрельба в CS имеет свойство быстро выходить из под контроля, поэтому стрелять необходимо осторожно. Чем дальше противник, тем меньше и реже должны быть выстреливаемые очереди. Если противник близко, то его можно и "зажать", но если он на другом конце "длины" на карте de_dust2, то необходимо стрелять одиночными (отпускать кнопку огня после каждого выстрела и нажимать снова, стреляя тем самым реже). Это касается не только пулемётов, но и пистолетов. Я и сам частенько слишком быстро и нетерпеливо нажимаю кнопку огня, стреляя из пистолета, из-за этого все пули пролетают над головой противника и меня благополучно наказывают за глупую ошибку.

Много полезных вещей о стрельбе говорится в "этом":http://www.youtube.com/watch?v=phcL7JqRBkM видео, поэтому рекомендую всем начинающим учиться стрелять посмотреть его.

И ещё. Старайтесь не бегать во время стрельбы. Во время движения разброс увеличивается, поэтому всегда останавливайтесь перед началом стрельбы. Чтобы это сделать быстро, вы можете тормозить себя обратным движением, например, если вы двигаетесь вправо, чтобы резко остановиться, нажмите влево. Особенно сильно это правило касается AWP - нужно быть абсолютно неподвижным при стрельбе из этого оружия, иначе вероятность Вашего попадания значительно уменьшается.

Самый популярный и универсальный способ научиться стрелять в CS - это игра на CSDM-серверах. На них вам придётся стрелять постоянно, поэтому Вы довольно быстро отточите это умение. Лично я чаще всего играю на *csdm.enjoygame.ru* - хороший сервер с хорошей стрельбой, only steam и подходящим количеством игроков. Почти всегда забит, но, к счастью, в новых версиях CS есть функция автоматического подключения к серверу в случае освобождения слота.

Кстати. Я был в восторге от "этой":http://2po.eu/blog/polnoe-rukovodstvo-po-uluchsheniyu-aima/ статьи, настоятельно рекомендую прочесть.

h2(#mixes). Миксы

Микс - это обычно пятеро знакомых (или даже не знакомых) человек, собравшихся, чтобы провести игру по всем правилам против такого же количества игроков. 15 раундов за одну сторону, после этого игроки переходят за другую. Выигрывает та команда, которая первая набирает в сумме 16 побед.

Любая команда изначально является миксом ("смесь" по-английски). И каждый игрок начинает именно с миксов. Собравшиеся, чтобы поиграть за одну сторону, люди ещё не являются командой. Они как минимум плохо понимают друг друга. Но есть несколько правил, которых нужно придерживаться, чтобы хоть как-то укротить неуправляемое стадо игроков.

Самые популярные карты разбиты на области и точки, которые имеют свои названия. Это необходимо для того, чтобы сообщить своим друзьям о том, где располагаются силы противника или бомба. Запоминаются эти названия лучше всего по мере игры, но лучше предварительно просмотреть эти названия где-нибудь в гугле, например, "тут":http://cobra.lv/blog/2009-02-04-59. Запомнив их, ВСЕГДА сообщайте своим тимейтам *как можно больше информации* о противнике и бомбе. Это первое правило командной игры.

Слушайте, что говорят Вам. Старайтесь выполнять просьбы ваших друзей. Делитесь по точкам, не бегайте вчетвером на A и втроём на B.

Если Вы проиграли раунд и у Вас мало денег (например, всего 2000$ или меньше), то покупать какой-нибудь дробовик или другую "пукалку" смысла нет и лучше один раунд "слить", а потом купить нормальное оружие. Но, вполне ясно, что неэффективно в одиночку слиться, в то время как все остальные бегают с "закупом". Сливаться необходимо всей командой. Если хотя бы у одного из игроков не хватает денег на нормальный закуп, и никто не может дать ему оружие ("дроп"), то нужно объявить слив для всей команды. Чтобы было проще определиться с этим, в начале раунда каждому игроку нужно писать в командном чате (кнопка "u") первые две цифры (или количество сотен) его денег. То есть, если у вас 5820$, пишите 58. Если 16000$, то 160. Так сразу будет просто определить, сколько игроков способны дать дроп, а скольким он необходим, и если оружий на всех не хватает, разумно один раунд слить. Если Вы пишете 0 в командном чате, это означает "слив", а если 1 - "закуп".

Не будьте слишком молчаливым, но и не болтайте слишком много. Поверьте, никому не интересно слушать, какой козёл и пидр тот, кто Вас убил. Зато им очень важно знать, где он сейчас находится. И важно знать как можно быстрее - говорите сразу же, не делайте дыхательных упражнений перед тем, как дать тимейтам эту информацию.

h2(#teamplay). Командная игра

В этом разделе не смогу сказать ничего нового, я ещё сам крайне неопытен в этом, так как ещё пока у меня не получилось создать сильную команду. Возможно, и не получится. Но я бы порекомендовал интересующимся этой темой прочитать эту новую статью, мне она очень и очень понравилась: "Общение - основа Counter Strike тимплея":http://www.cs.aplus.by/index.php?newsid=73
